package me.travisjoe.barrel.tileentity;

import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;

/**
 * Created by travis on 5/21/16.
 */
public class BarrelTileEntity extends TileEntity {
    private Item item;
    private int itemCount;

    public Item getItem() {
        return item;
    }

    public int getAmount() {
        return itemCount;
    }

    public boolean addItem(ItemStack stack) {
        // empty hand
        if(stack == null) {
            return false;
        }
        // no existing item
        if(item == null) {
            item = stack.getItem();
            itemCount = stack.stackSize;
            return true;
        } else if(item.equals(stack.getItem())){    // existing item and stack are equal
            itemCount += stack.stackSize;
            return true;
        }

        return false;
    }

    // Loading and saving the data to NBT, so the info is not lost when the world is reloaded.

    @Override
    public void readFromNBT(NBTTagCompound compound) {
        super.readFromNBT(compound);
        String name = compound.getString("item");
        item = Item.getByNameOrId(name);
        itemCount = compound.getInteger("amount");
    }

    @Override
    public void writeToNBT(NBTTagCompound compound) {
        super.writeToNBT(compound);
        compound.setString("item", item.getUnlocalizedName());
        compound.setInteger("amount", itemCount);
    }
}
