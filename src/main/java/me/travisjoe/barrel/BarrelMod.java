package me.travisjoe.barrel;

import me.travisjoe.barrel.blocks.BarrelBlock;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventHandler;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;

/*
 *   Adds a basic block which can store (for now) unlimited amounts of a single item.
 *   TODO: Add the ability to remove items from the barrel
 *   TODO: Add a crafting recipe
 *   TODO: Limit the amount of items in the barrel
 *   TODO: Drop the items in the barrel when it is broken. Alternatively, allow the items to remain inside when broken.
 */

@Mod(modid = BarrelMod.MODID, version = BarrelMod.VERSION)
public class BarrelMod
{
    public static final String MODID = "Barrels";
    public static final String VERSION = "1.0";

    @EventHandler
    public void init(FMLInitializationEvent event)
    {
        BarrelBlock barrel = new BarrelBlock();
    }
}
