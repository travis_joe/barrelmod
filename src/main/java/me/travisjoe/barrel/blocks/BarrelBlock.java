package me.travisjoe.barrel.blocks;

import me.travisjoe.barrel.tileentity.BarrelTileEntity;
import net.minecraft.block.Block;
import net.minecraft.block.ITileEntityProvider;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemBlock;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.world.World;
import net.minecraftforge.fml.common.registry.GameRegistry;

/**
 * Created by travis on 5/21/16.
 */
public class BarrelBlock extends Block implements ITileEntityProvider {

    public BarrelBlock() {
        super(Material.iron);
        setUnlocalizedName("barrelBlock");
        setRegistryName("barrelblock");
        setCreativeTab(CreativeTabs.tabBlock);
        GameRegistry.register(this);
        GameRegistry.register(new ItemBlock(this), getRegistryName());
        GameRegistry.registerTileEntity(BarrelTileEntity.class, "barrel");
        setHardness(10.0f);
        setResistance(10.0f);
        setHarvestLevel("axe", 0);
    }

    @Override
    public TileEntity createNewTileEntity(World worldIn, int meta) {
        return new BarrelTileEntity();
    }

    // Get the entity that was clicked
    private BarrelTileEntity getTileEntity(World world, BlockPos position) {
        return (BarrelTileEntity) world.getTileEntity(position);
    }

    // Called when the block is right clicked
    @Override
    public boolean onBlockActivated(World worldIn, BlockPos pos, IBlockState state, EntityPlayer playerIn, EnumHand hand, ItemStack heldItem, EnumFacing side, float hitX, float hitY, float hitZ) {
        super.onBlockActivated(worldIn, pos, state, playerIn, hand, heldItem, side, hitX, hitY, hitZ);

        BarrelTileEntity entity = getTileEntity(worldIn, pos);

        if(!worldIn.isRemote) {
            if(entity.addItem(heldItem)) {
                playerIn.addChatMessage(new TextComponentString("Added item! Barrel info: "));
                playerIn.addChatMessage(new TextComponentString("Type: " + entity.getItem().getUnlocalizedName() + " Amount: " + entity.getAmount()));
                heldItem = null;
            } else {
                // There will be an NPE here if you try clicking with an empty hand
                playerIn.addChatMessage(new TextComponentString("Barrel contains " + entity.getAmount() + " " + entity.getItem().getUnlocalizedName()));
            }
        }

        return true;
    }
}
